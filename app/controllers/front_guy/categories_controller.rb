module FrontGuy
  class CategoriesController < ApplicationController

    def new
      @category = FrontGuy::Category.new
    end

    def edit
      @category = FrontGuy::Category.find(params[:id])
    end

    def create
      @category = FrontGuy::Category.new(params)
      @category.save
      redirect_to category_components_path(@category)
    end
  end
end
