module FrontGuy
  class ExamplesController < ApplicationController
    def create
      @example = FrontGuy::Example.new(params)
      @example.save
      redirect_to components_path
    end

    def new
      @example = FrontGuy::Example.new(params)
    end

    def edit
      @example = FrontGuy::Example.find(params[:id])
    end

    def create
      @example = FrontGuy::Example.new(params)
      @example.save
      redirect_to category_components_path(@example.component.category_id)
    end
  end
end
