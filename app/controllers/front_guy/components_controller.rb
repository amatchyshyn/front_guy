module FrontGuy
  class ComponentsController < ApplicationController

    layout 'front_guy'

    def index
      @categories = FrontGuy::Category.all
      @category = FrontGuy::Category.current_or_primary(params)
      return unless @category
      @components = @category.components
    end

    def new
      @component = FrontGuy::Component.new(params)
    end

    def edit
      @component = FrontGuy::Component.find(params[:id])
    end

    def create
      @component = FrontGuy::Component.new(params)
      @component.save
      redirect_to category_components_path(@component.category_id, uid: @component.uid)
    end
  end
end
