FrontGuy::Engine.routes.draw do
  resources :categories do
    resources :components
  end

  resources :components do
    resources :examples
  end



  root to: 'components#index'
  get '/:id' => 'components#index'
end
