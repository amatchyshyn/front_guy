#bin/rails generate front_guy
class FrontGuyGenerator < Rails::Generators::Base
  def create_initializer_file
    # create_file "config/initializers/frontend_guide.rb", "# Add initialization content here"
    create_file ".front_guy.yml", FrontGuy::Defaults::GENERATOR
  end
end
