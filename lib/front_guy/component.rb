module FrontGuy
  class Component < FrontGuy::Base
    attr_accessor :category_id, :title, :uid

    def initialize(opts = {})
      @uid =  opts[:uid] || opts[:id] ||  Base.radom_uid
      @category_id = opts[:category_id]
      @title = opts[:title]
    end

    def examples
      examples_list = @@guide[:examples].select { |c| c[:component_id] == @uid }
      examples_list.map { |c| Example.new(c) }
    end

    def save
      new_component = { uid: @uid,
                        category_id: @category_id,
                        title: @title }
      component_hash = @@guide[:components].find { |c| c[:uid] == @uid }
      if component_hash
        component_hash[:title] = new_component[:title]
      else
        @@guide[:components] << new_component
      end
      Base.save
    end

    class << self
      def all
        @@guide[:components]
      end

      def from_category(category_id) #todo move this ti category
        return [] unless @@guide[:components].any?
        category_components = @@guide[:components].select { |h| h[:category_id] == category_id }
        category_components.map { |c| self.new(category_id: c[:category_id],title: c[:title], uid: c[:uid]) }
      end
    end
  end
end
