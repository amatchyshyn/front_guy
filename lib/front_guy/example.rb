module FrontGuy
  class Example < FrontGuy::Base
    attr_accessor :uid, :component_id, :title, :description, :body, :language

    def initialize(opts = {})
      @uid = opts[:uid] || opts[:id] || Base.radom_uid
      @component_id = opts[:component_id]
      @title = opts[:title]
      @description = opts[:description]
      @body = opts[:body]
      @language = opts[:language]
    end

    def component
      Component.new(@@guide[:components].find { |c| c[:uid] == @component_id })
    end

    def save
      new_example = { uid: @uid,
                      title: @title,
                      description: @description,
                      body: @body,
                      language: @language,
                      component_id: @component_id }

      example_hash = @@guide[:examples].find { |c| c[:uid] == @uid }
      if example_hash
        example_hash[:title] = new_example[:title]
        example_hash[:description] = new_example[:description]
        example_hash[:body] = new_example[:body]
        example_hash[:language] = new_example[:language]
      else
        @@guide[:examples] << new_example
      end
      Base.save
    end

    class << self
      def all
        @@guide[:examples]
      end
    end
  end
end
