module FrontGuy
  class Base
    GUIDE_FILE_NAME = ".front_guy.yml"
    # TODO hold exceptions
    @@guide = begin
                YAML.load_file(FrontGuy::Base::GUIDE_FILE_NAME)
              rescue
                p 'please run front_guy initializer'
              end

    def to_s
      @uid
    end

    class << self
      def find(uid)
        new(@@guide[name.demodulize.tableize.to_sym].find { |c| c[:uid] == uid })
      end

      def radom_uid
        SecureRandom.urlsafe_base64
      end

      def save
        File.open(GUIDE_FILE_NAME, 'w') do |h|
          h.write @@guide.to_yaml
        end
      end
    end
  end
end
