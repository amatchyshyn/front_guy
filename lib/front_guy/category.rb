module FrontGuy
  class Category < FrontGuy::Base
    attr_accessor :uid, :title, :index

    def initialize(opts = {})
      @uid = opts[:uid] || opts[:id] || Base.radom_uid
      @title = opts[:title]
    end

    def save
      new_category = { uid: @uid,
                       title: @title,
                       index: @index || nex_index }

      category_hash = @@guide[:categories].find { |c| c[:uid] == @uid }
      if category_hash
        category_hash[:title] = new_category[:title]
      else
        @@guide[:categories] << new_category
      end
      Base.save
    end

    def components
      components_list = @@guide[:components].select { |c| c[:category_id] == @uid }
      components_list.map{ |c| Component.new(c) }
    end

    class << self
      def all
        @@guide[:categories].map{ |c| self.new(c) }
      end

      def current_or_primary(opts = {})
        return unless @@guide[:categories].any?
        category = if opts[:category_id].present?
                     @@guide[:categories].find { |h| h[:uid] == opts[:category_id] }
                   else
                     @@guide[:categories].find { |h| h[:index] == 0 }
                   end
        self.new(category)
      end
    end

    private

    def nex_index
      @@guide[:categories].count
    end
  end
end
# .map.with_index
