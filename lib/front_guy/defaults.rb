module FrontGuy
  module Defaults
    GENERATOR = {
      components: [],
      examples: [],
      categories: []
    }.freeze.to_yaml
  end
end
