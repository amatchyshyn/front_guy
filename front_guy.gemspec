$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "front_guy/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "front_guy"
  s.version     = FrontGuy::VERSION
  s.authors     = ["Andriy Matchyshyn"]
  s.email       = ["blasterun@gmail.com"]
  s.homepage    = "TODO"
  s.summary     = "TODO: Summary of FrontGuy."
  s.description = "TODO: Description of FrontGuy."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.7.1"

  s.add_development_dependency "mysql2"
end
